const pWelcome = document.querySelector("#p-welcome");
const getName = () => {
  const name = prompt("Как Вас зовут", "");
  if (name) {
    return `, ${name}`;
  }
  return "";
};

pWelcome.textContent += `Hi${getName()}! It's a new course for you!`;
